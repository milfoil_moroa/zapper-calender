 // TODO : FI

        class DatePicker {



            calender = {
                'today': this.formatDate(new Date()),
                'month': [
                    { 'long': 'January', 'short': 'Jan' },
                    { 'long': 'February', 'short': 'Feb' },
                    { 'long': 'March', 'short': 'Mar' },
                    { 'long': 'April', 'short': 'Apr', },
                    { 'long': 'May', 'short': 'May' },
                    { 'long': 'June', 'short': 'Jun' },
                    { 'long': 'July', 'short': 'Jul' },
                    { 'long': 'August', 'short': 'Aug' },
                    { 'long': 'September', 'short': 'Sep' },
                    { 'long': 'October', 'short': 'Oct' },
                    { 'long': 'November', 'short': 'Nov' },
                    { 'long': 'December', 'short': 'Dec' },
                ],
                'weekdays': [
                    { 'long': 'Sunday', 'short': 'Sun', 'shorter': 'S' },
                    { 'long': 'Monday', 'short': 'Mon', 'shorter': 'M' },
                    { 'long': 'Tuesday', 'short': 'Tue', 'shorter': 'T' },
                    { 'long': 'Wednesday', 'short': 'Wed', 'shorter': 'W' },
                    { 'long': 'Thursday', 'short': 'Thu', 'shorter': 'T' },
                    { 'long': 'Friday', 'short': 'Fri', 'shorter': 'F' },
                    { 'long': 'Saturday', 'short': 'Sat', 'shorter': 'S' },

                ],

                'config': {

                    'format': {
                        'month': 'short',
                        'weekdays': 'short'
                    },
                    'style': {}
                }



            };
            selectedDateRange = { 'start': null, 'end': null };
            selectedDate = '';
            dateRange = [];
            constructor() {

            }



            static isAfterDay(range) {
                return (this.parseToTime(range.start) > this.parseToTime(range.end));
            }

            static isBeforeDay() {
                return (this.parseToTime(range.start) < this.parseToTime(range.end));
            }

            static isSameDay(range) {
                return (
                    range.start.getFullYear() === range.end.getFullYear() &&
                    range.start.getMonth() === range.end.getMonth() &&
                    range.start.getDate() === range.end.getDate()
                );
            }

            static isDayWithInRange(dateRange) {
                return (dateRange.value >= dateRange.min && dateRange.value <= dateRange.max);
            }

            static compareDate = (start, end) => {
                range = { 'start': start, 'end': end }
                if (this.isBeforeDay(range)) {
                    return { 'start': start, 'end': end }
                } else if (isAfterDay(range)) {
                    return { 'start': end, 'end': start }
                }
            }

            static parseToTime = (date) => {
                return new Date(date).getTime();
            }

            static parseToDate = (time) => {
                return new Date(time);
            }


            static formatDate = date => {
                let day = date.getDate();
                if (day < 10) {
                    day = '0' + day;
                }

                let month = date.getMonth() + 1;
                if (month < 10) {
                    month = '0' + month;
                }

                year = date.getFullYear();
                return day + '-' + month + '-' + year;
            }


            static clearCalender = () => {
                this.dateRange = []; // reset the date ranage
                let daysUI = document.querySelectorAll('td');
                if (daysUI && daysUI.length > 0) {
                    daysUI.forEach(el => {
                        this.removeClass(el, 'active');
                        this.removeClass(el, 'between');
                    });
                }
            }

            static toggleClass = (element, className) => {
                element.classList.toggle(className);
            }

            static removeClass = (element, className) => {
                element.classList.remove(className);
            }

            static addClass = (element, className) => {
                element.classList.add(className);
            }

            static highlightSelectedDate = (e) => {

                el = e.target;
                let _id = el.dataset.id;
                if (!this.isBeforeDay(_id)) {
                    _dateRange.push(this.parseToTime(_id));
                    if (!this.getStartDate()) {

                        setStartDate(_id);
                        toggleClass(el, 'active');
                        console.log('1. SET START ', this.getStartDate());
                    } else if (parseToTime(this.getStartDate()) && this.isSameDay({ 'start': parseToDate(this.getStartDate()), 'end': parseToDate(_id) })) {
                        setStartDate(null);
                        this.removeClass(el, 'active');

                        console.log('0. UNSET START ', this.getStartDate());
                    } else {
                        if (!this.getEndDate()) {
                            res = compareDate(_id, this.getStartDate());
                            this.setEndDate(res.end);
                            this.setSelectedDateRange(res);
                            this.highlightInBetweenDates();

                            console.log('2. SET END ', this.getStartDate());

                        } else {
                            this.clearCalender();
                            this.setDateRange(this.parseToTime(_id));
                            this.setSelectedDateRange({ 'start': _id, 'end': null });
                            this.toggleClass(el, 'active');
                            console.log('3. RESET START ', this.getStartDate());
                        }
                    }
                }


                console.log('COMPUTE RANGE : ', this.getSelectedDateRange());
                console.log('DATE RANGE : ', this.getDateRange());

            }

            clearHighlight = () => {
                let daysUI = document.querySelectorAll('td.between');
                if (daysUI && daysUI.length > 0) {
                    daysUI.forEach(el => {
                        this.removeClass(el, 'between');
                    });
                }
            }

            static highlightInBetweenDates = () => {
                let daysUI = document.querySelectorAll('td.selectable');
                if (daysUI && daysUI.length > 0) {
                    daysUI.forEach(el => {
                        _dateRange = {
                            'min': this.parseToTime(this.getStartDate()),
                            'max': this.parseToTime(this.getEndDate()),
                            'value': this.parseToTime(el.dataset.id),
                        }
                        // add class when day is within range
                        if (dateInRange(_dateRange)) {
                            this.addClass(el, 'between');
                        }

                        if (_dateRange.max === _dateRange.value || _dateRange.min === _dateRange.value) {
                            this.addClass(el, 'active');
                        }
                    });
                }

            }


            static getStartDate() {
                return this.selectedDateRange.start;
            }

            static getEndDate() {
                return this.selectedDateRange.end;
            }


            setStartDate(date) {
                this.selectedDateRange.start = date;
            }

            setEndDate(date) {
                this.selectedDateRange.end = date;
            }

            getDateRange(date = null) {
                if (!date) {
                    return this.selectedDateRange;
                }
                return this.selectedDateRange[date];
            }

            setDateRange(date) {
                this.selectedDateRange.push(date);
            }


            setSelectedDateRange(dateRange) {
                setStartDate(dateRange.start);
                setEndDate(dateRange.end);
                this.selectedDateRange = dateRange;
            }


            getSelectedDateRange() {
                return this.selectedDateRange;
            }




        }



        class CalenderComponent extends DatePicker {


            year;
            month;
            day;
            date;

            constructor(date = this.calender.today) {
                super()
                setDate(date);
                initCalender();
            }


            setDate = (date) => {
                this.date = date;
            }

            getDate = () => {
                return this.date;
            }


            setDay = (day) => {
                this.day = day;
            }


            getDay = () => {
                return this.day;
            }



            setYear = (year) => {
                this.year = year;
            }

            getYear = () => {
                return this.year
            }

            setMonth = (month) => {
                this.month = month;
            }

            getMonth = () => {
                return this.month
            }

            getFirstDay = () => {
                return (new Date(this.getYear(), this.getMonth())).getDay();
            }

            getDaysInMonth = () => {
                return 32 - new Date(ythis.getYear(), this.getMonth(), 32).getDate();
            }


            createAttribute = (element, attr) => {
                _attr = document.createAttribute(Object.keys(attr).shift());
                _attr.value = Object.values(attr).shift();
                element.setAttributeNode(_attr);
            }


            renderYearWidget = () => {
                document.querySelector('#year').textContent = this.getYear();
            }

            renderMonthWidget = (month) => {
                document.querySelector('#month').textContent = this.calender.months[this.getMonth()].long;
            }

            renderDayWidget = (day) => {
                document.querySelector('#day').textContent = day;
            }


            renderRightMonthWidget = () => {
                document.querySelector('.right-wrapper .month').textContent = this.calender.months[this.getMonth()].long;
            }


            renderWeekdaysWidget = () => {

                monthFormat = this.calender.config.format.month;
                let calenderEl = document.querySelector('#calender-board');

                // let calenderUI = document.getElementsByClassName('calender');
                let th = calenderEl.createTHead();  // -> create calender header
                let tr = th.insertRow(); // -> insert into row

                tr.classList.add("weekdays");   // --> attach weekdays class

                // --> populate the weekdays
                this.calender.weekdays.forEach((weekday, index) => {

                    let th = document.createElement("th");
                    let text = document.createTextNode(weekday[monthFormat]);
                    th.appendChild(text);
                    tr.appendChild(th);

                    let attributes = [{ 'data-weekday': weekday[monthFormat] }, { 'data-column': index }];

                    // --> add weekdays attributes
                    attributes.forEach(attr => {
                        this.createAttribute(th, attr);
                        // _attr = document.createAttribute(Object.keys(attr).shift());
                        // _attr.value = Object.values(attr).shift();
                        // th.setAttributeNode(_attr);
                    });

                    tr.appendChild(th)
                });

                calenderEl.append(th);

            }
            renderDaysWidget = () => {


                renderRightMonthWidget();
                renderDayWidget();
                renderMonthWidget();
                renderYearWidget();

                let calenderEl = document.querySelector('#calender-board');
                let tb = document.createElement('tbody');

                // clearing all previous cells
                calenderEl.innerHTML = "";

                // creating all cells
                let day = 1;
                for (let i = 0; i < 6; i++) {
                    // creates a table row
                    let tr = document.createElement("tr");

                    let attributes = [{ 'class': 'days' }, { 'data-row': i }];

                    // --> add days attributes
                    attributes.forEach(attr => {

                        this.createAttribute(tr, attr);
                        // _attr = document.createAttribute(Object.keys(attr).shift());
                        // _attr.value = Object.values(attr).shift();
                        // tr.setAttributeNode(_attr);
                    });

                    //creating individual cells, filing them up with data.
                    this.calender.weekdays.forEach((weekday, index) => {
                        if (i === 0 && index < firstDay) {
                            let td = document.createElement("td");
                            let text = document.createTextNode("");
                            this.createAttribute(tr, { 'data-column': index });
                            // _attr = document.createAttribute();
                            // _attr.value = index;


                            // tr.setAttributeNode(_attr);

                            td.appendChild(text);
                            tr.appendChild(td);

                        }

                        else if (day > this.getDaysInMonth()) {
                            return;
                        }


                        else {

                            let td = document.createElement("td");
                            let text = document.createTextNode(day);

                            td.appendChild(text);


                            // create date as id for calender column
                            let _date = day + '-' + this.calender.months[this.getMonth()].short + '-' + this.getYear();
                            let attributes = [{ 'class': 'selectable' }, { 'data-column': index }, { 'data-id': _date }];

                            // --> add days attributes
                            attributes.forEach(attr => {

                                this.createAttribute(td, attr);
                                // _attr = document.createAttribute(Object.keys(attr).shift());
                                // _attr.value = Object.values(attr).shift();
                                // td.setAttributeNode(_attr);
                            });

                            // highlight today
                            if (isDateToday(_date)) {
                                td.classList.add('currentDay');
                            }

                            // highlight the selected dates when regenerating the calender days
                            if (this.getDateRange().includes(parseToTime(_date))) {

                                this.addClass(td, 'active');
                                // td.classList.add('active');
                            }

                            // add select date handler 
                            td.addEventListener('click', (e) => selectDayHandler(e));
                            tr.appendChild(td);
                            day++;
                        }

                    });

                    tb.appendChild(tr);
                    calenderEl.appendChild(tb); // appending each row into calendar body.
                }


            }



            initCalender() {

                setYear(date.getFullYear());
                setMonth(date.getMonth());
                setDay(date.getDate());


                renderDaysWidget();

                renderWeekdaysWidget();
            }



        }

        var monthDirection = 0;
        let _dateRange = [];
        getNextMonth = () => {
            monthDirection++;
            var current;
            var now = new Date();
            if (now.getMonth() == 11) {
                current = new Date(now.getFullYear() + monthDirection, 0, 1);
            } else {
                current = new Date(now.getFullYear(), now.getMonth() + monthDirection, 1);
            }
            initCalender(current);
        }
        getPrevMonth = () => {
            monthDirection--;
            var current;
            var now = new Date();
            if (now.getMonth() == 11) {
                current = new Date(now.getFullYear() + monthDirection, 0, 1);
            } else {
                current = new Date(now.getFullYear(), now.getMonth() + monthDirection, 1);
            }
            initCalender(current);
        }





        initCalender = new CalenderComponent();


        document.querySelector('.fa-angle-double-right').addEventListener('click', (e) => {
            document.querySelector(".right-wrapper").classList.toggle("is-active");
            e.target.classList.toggle("is-active");
        });


        document.querySelector(".fa-angle-left").addEventListener('click', () => {
            getPrevMonth();
            document.querySelector(".main").classList.add("is-rotated-left");
            setTimeout(function () {
                document.querySelector(".main").classList.remove("is-rotated-left");
            }, 195);
        });

        document.querySelector(".fa-angle-right").addEventListener('click', () => {
            getNextMonth();
            document.querySelector(".main").classList.add("is-rotated-right");
            setTimeout(function () {
                document.querySelector(".main").classList.remove("is-rotated-right");
            }, 195);
        });
