var monthDirection = 0;
let  _dateRange = [];
getNextMonth = () => {
    monthDirection++;
    var current;
    var now = new Date();
    if (now.getMonth() == 11) {
        current = new Date(now.getFullYear() + monthDirection, 0, 1);
    } else {
        current = new Date(now.getFullYear(), now.getMonth() + monthDirection, 1);
    }
    initCalender(current);
}
 getPrevMonth = () => {
    monthDirection--;
    var current;
    var now = new Date();
    if (now.getMonth() == 11) {
        current = new Date(now.getFullYear() + monthDirection, 0, 1);
    } else {
        current = new Date(now.getFullYear(), now.getMonth() + monthDirection, 1);
    }
    initCalender(current);
}


Date.prototype.isSameDateAs = function (pDate) {
    return (
        this.getFullYear() === pDate.getFullYear() &&
        this.getMonth() === pDate.getMonth() &&
        this.getDate() === pDate.getDate()
    );
};



 clearCalender = () => {
    _dateRange = []; // reset the date ranage
    let daysUI = document.querySelectorAll('td');
    if (daysUI && daysUI.length > 0) {
        daysUI.forEach(el => {
            el.classList.remove('between');
            el.classList.remove('active');
        });
    }
}



const CALENDER = {

    'today': new Date(),
    'months': [
        { 'long': 'January', 'short': 'Jan' },
        { 'long': 'February', 'short': 'Feb' },
        { 'long': 'March', 'short': 'Mar' },
        { 'long': 'April', 'short': 'Apr', },
        { 'long': 'May', 'short': 'May' },
        { 'long': 'June', 'short': 'Jun' },
        { 'long': 'July', 'short': 'Jul' },
        { 'long': 'August', 'short': 'Aug' },
        { 'long': 'September', 'short': 'Sep' },
        { 'long': 'October', 'short': 'Oct' },
        { 'long': 'November', 'short': 'Nov' },
        { 'long': 'December', 'short': 'Dec' },
    ],
    'weekdays': [
        { 'long': 'Sunday', 'short': 'Sun', 'shorter': 'S' },
        { 'long': 'Monday', 'short': 'Mon', 'shorter': 'M' },
        { 'long': 'Tuesday', 'short': 'Tue', 'shorter': 'T' },
        { 'long': 'Wednesday', 'short': 'Wed', 'shorter': 'W' },
        { 'long': 'Thursday', 'short': 'Thu', 'shorter': 'T' },
        { 'long': 'Friday', 'short': 'Fri', 'shorter': 'F' },
        { 'long': 'Saturday', 'short': 'Sat', 'shorter': 'S' },
    ],
    'config': {
        'format': {
            'month': 'short',
            'weekdays': 'short'
        },
        'style': {}
    }



};



formatDate = (date, separator = '-') => {
    return date.getDate() + separator + CALENDER.months[date.getMonth()].short + separator + date.getFullYear();
}

todayDate = () => {
    return formatDate(CALENDER.today);
}

var _selectedDate = { 'start': '', 'end': null };

isDateBefore = (date, today = todayDate()) => {
    return stringToTime(date) < stringToTime(today);
}
isDateAfter = (date, today = todayDate()) => {
    return stringToTime(date) > stringToTime(today);
}

compareDate = (start, end) => {
    if (isDateBefore(start, end)) {
        return { 'start': start, 'end': end }
    } else if (isDateAfter(start, end)) {
        return { 'start': end, 'end': start }
    }


}
isDateToday = (date) => {
    return stringToTime(date) === stringToTime(todayDate());
}

stringToTime = (date) => {
    return new Date(date).getTime();
}

timeToDate = (time) => {
    return new Date(time);
}

dateInRange = (min, max, value) => {
    return value >= min && value <= max;
}

highlightSelectedDays = () => {
    let daysUI = document.querySelectorAll('td.selectable');
    if (daysUI && daysUI.length > 0) {
        daysUI.forEach(el => {
            _minDate = stringToTime(_selectedDate.start);
            _maxDate = stringToTime(_selectedDate.end);
            _uiDate = stringToTime(el.dataset.id);

        
            if (dateInRange(_minDate, _maxDate, _uiDate)) {
                el.classList.add('between')
            }

            if(_maxDate === _uiDate || _minDate === _uiDate){
                el.classList.add('active');
            }
        });
    }
};


removeHighlight = () => {
    let daysUI = document.querySelectorAll('td.between');
    if (daysUI && daysUI.length > 0) {
        daysUI.forEach(el => {
            el.classList.remove('between');
        });
    }
}
resetDateRange = () => {
    _dateRange = []; // reset the date ranage
    let daysUI = document.querySelectorAll('td');
    if (daysUI && daysUI.length > 0) {
        daysUI.forEach(el => {
            el.classList.remove('between');
            el.classList.remove('active');
        });
    }
}


selectDayHandler = (e) => {
    el = e.target;
    let _id = el.dataset.id;
    if (!isDateBefore(_id)) {
        _dateRange.push(stringToTime(_id));
        if (!_selectedDate.start) {
           
            _selectedDate.start = _id;
            el.classList.toggle("active");
            console.log('1. SET START ', _selectedDate.start);
        } else if( stringToTime(_selectedDate.start) && stringToTime(_selectedDate.start) === stringToTime(_id)){
            _selectedDate.start = null;
            el.classList.remove("active");
        } else {
            if (!_selectedDate.end) {
                _selectedDate = compareDate(_id, _selectedDate.start);
                highlightSelectedDays();
            } else {
                removeHighlight();
                resetDateRange();
                _dateRange.push(stringToTime(_id));
                _selectedDate = { 'start': _id, 'end': null };
                el.classList.toggle("active");
            }
        }
    }
}



weekdaysWidget = () => {

    monthFormat = CALENDER.config.format.month;
    let calenderEl = document.querySelector('#calender-board');

    // let calenderUI = document.getElementsByClassName('calender');
    let th = calenderEl.createTHead();  // -> create calender header
    let tr = th.insertRow(); // -> insert into row

    tr.classList.add("weekdays");   // --> attach weekdays class

    // --> populate the weekdays
    CALENDER.weekdays.forEach((weekday, index) => {

        let th = document.createElement("th");
        let text = document.createTextNode(weekday[monthFormat]);
        th.appendChild(text);
        tr.appendChild(th);

        let attributes = [{ 'data-weekday': weekday[monthFormat] }, { 'data-column': index }];

        // --> add weekdays attributes
        attributes.forEach(attr => {
            _attr = document.createAttribute(Object.keys(attr).shift());
            _attr.value = Object.values(attr).shift();
            th.setAttributeNode(_attr);
        });

        tr.appendChild(th)
    });

    calenderEl.append(th);

}
daysWidget = (date) => {


    year = date.getFullYear();
    month = date.getMonth();


    yearEl = document.querySelector('#year').textContent = year;
    monthEl = document.querySelector('#month').textContent = CALENDER.months[month].long;

    dayEl = document.querySelector('#day').textContent = date.getDate();

    _monthEl = document.querySelector('.right-wrapper .month').textContent = CALENDER.months[month].long;
   


    let firstDay = (new Date(year, month)).getDay();
    let daysInMonth = 32 - new Date(year, month, 32).getDate();

    let calenderEl = document.querySelector('#calender-board');
    let tb = document.createElement('tbody');


    // clearing all previous cells
    calenderEl.innerHTML = "";

    // creating all cells
    let day = 1;
    for (let i = 0; i < 6; i++) {
        // creates a table row
        let tr = document.createElement("tr");

        let attributes = [{ 'class': 'days' }, { 'data-row': i }];

        // --> add days attributes
        attributes.forEach(attr => {
            _attr = document.createAttribute(Object.keys(attr).shift());
            _attr.value = Object.values(attr).shift();
            tr.setAttributeNode(_attr);
        });

        //creating individual cells, filing them up with data.
        CALENDER.weekdays.forEach((weekday, index) => {
            if (i === 0 && index < firstDay) {
                let td = document.createElement("td");
                let text = document.createTextNode("");
                _attr = document.createAttribute('data-column');
                _attr.value = index;


                tr.setAttributeNode(_attr);

                td.appendChild(text);
                tr.appendChild(td);

            } 
        
             else if (day > daysInMonth) {
                return;
            } 
            
            
            else {

                let td = document.createElement("td");
                let text = document.createTextNode(day);

                td.appendChild(text);


                // create date as id for calender column
                let _date = day + '-' + CALENDER.months[month].short + '-' + year;
                let attributes = [{ 'class': 'selectable' }, { 'data-column': index }, { 'data-id': _date }];

                // --> add days attributes
                attributes.forEach(attr => {
                    _attr = document.createAttribute(Object.keys(attr).shift());
                    _attr.value = Object.values(attr).shift();
                    td.setAttributeNode(_attr);
                });

                // highlight today
                if (isDateToday(_date)) {
                    td.classList.add('currentDay');
                }

                // highlight the selected dates when regenerating the calender days
                if (_dateRange.includes(stringToTime(_date))) {
                    td.classList.add('active');
                }

                // add select date handler 
                td.addEventListener('click', (e) => selectDayHandler(e));
                tr.appendChild(td);
                day++;
            }




        });

        tb.appendChild(tr);


        calenderEl.appendChild(tb); // appending each row into calendar body.
    }


}


function initCalender(date = CALENDER.today) {
    // -> build the weekdays widget
   
    // clearCalender();
   
    daysWidget(date);

    weekdaysWidget();
}


initCalender();


document.querySelector('.fa-angle-double-right').addEventListener('click', (e) => {
    document.querySelector(".right-wrapper").classList.toggle("is-active");
    e.target.classList.toggle("is-active");
});


document.querySelector(".fa-angle-left").addEventListener('click',  () =>  {
    getPrevMonth();
    document.querySelector(".main").classList.add("is-rotated-left");
    setTimeout(function () {
        document.querySelector(".main").classList.remove("is-rotated-left");
    }, 195);
});

document.querySelector(".fa-angle-right").addEventListener('click',  () =>  {
    getNextMonth();
    document.querySelector(".main").classList.add("is-rotated-right");
    setTimeout(function () {
        document.querySelector(".main").classList.remove("is-rotated-right");
    }, 195);
});